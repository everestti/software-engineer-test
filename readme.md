# Avaliação prática - Software Engineer


## Instruções

1. Faça um fork do repositório (Como faço isso? [Passo 1](https://bytebucket.org/everestti/software-engineer-test/raw/852b88d2ba088ff3cc5b4ecbbedfe1a6a403fb59/assets/fork2.JPG) , [Passo 2](https://bytebucket.org/everestti/software-engineer-test/raw/852b88d2ba088ff3cc5b4ecbbedfe1a6a403fb59/assets/fork1.JPG) ), [Não tenho o GIT instalado](https://www.youtube.com/watch?v=TF7Vvk0s86g), [Nunca utilizei o GIT](https://try.github.io/levels/1/challenges/1)
2. Faça um clone do fork para sua máquina
3. Crie um projeto ASP.NET, onde a página principal funciona como um índice para a resolução dos problemas propostos.
4. Finalizada a resolução do projeto, adicione ao repositório os usuários cbfranca (Cleidson França) e lopes19 (Mateus Lopes) para que façamos a correção. ([Como faço isso?](https://bytebucket.org/everestti/software-engineer-test/raw/b811f9c39cbb9bf60a35f79da253fe84382b16b8/assets/invite.JPG))
5. Agora é só aguardar o nosso contato!


## Problema 1

Uma rei requisitou os serviços de um operário e disse-lhe que pagaria qualquer preço. O operário, que passava necessidades e previsava de alimentos , perguntou ao rei se o pagamento poderia ser feito com grãos de trigo dispostos em um tabuleiro de xadrez, de tal forma que o primeiro quadro deveria conter apenas um grão e os quadros subseqüentes , o dobro do quadro 
anterior. O rei achou o trabalho barato e pediu que o serviço fosse executado, sem se dar conta de que seria impossível efetuar o pagamento. Faça um algoritmo para calcular o número de grãos que o rei esperava receber.

## Problema 2    

Considere a tabela ```FILMES``` e seus dados:


![alt text]( https://bytebucket.org/everestti/software-engineer-test/raw/b811f9c39cbb9bf60a35f79da253fe84382b16b8/assets/sql.jpg "Sql table")

Deve ser executado o seguinte comando seguindo a sintaxe SQL padrão:

 a) SELECT f.NomeFilme, f.AnoLancamento
     FROM FILMES f
     ORDER BY f.CodProdutora
     HAVING f.PrecoFilme > (SELECT g.PrecoFilme
     FROM FILMES g WHERE (g.CodFilme = 3) and     
     (g.CodProdutora = 5)

 b) SELECT f.NomeFilme, f.AnoLancamento
    FROM FILMES f
    ORDER BY f.CodProdutora
    HAVING f.PrecoFilme > (SELECT g.PrecoFilme
    FROM FILMES g WHERE (g.CodFilme = 3) or
    (g.CodProdutora = 5)

 c) SELECT f.NomeFilme, f.AnoLancamento
    FROM FILMES f
    ORDER BY f.CodFilme
    HAVING f.PrecoFilme > (SELECT g.PrecoFilme
    FROM FILMES g WHERE (g.CodFilme = 3) or
    (g.CodProdutora = 5)

 d) SELECT f.NomeFilme, f.AnoLancamento
    FROM FILMES f
    ORDER BY f.CodProdutora
    HAVING f.PrecoFilme < (SELECT g.PrecoFilme
    FROM FILMES g WHERE (g.CodFilme = 3) or
    (g.CodProdutora = 5)

 e) SELECT f.NomeFilme, f.AnoLancamento    
    FROM FILMES f    
    ORDER BY f.CodProdutora    
    HAVING f.PrecoFilme <= (SELECT g.PrecoFilme    
    FROM FILMES g WHERE (g.CodFilme = 3) or
    (g.CodProdutora = 5)
    

## Problema 3

Um grupo de empreendedores resolvem projetar um celuar para pessoas com deficiência visual. Você foi contratado para desenvolver esse projeto. Escreva um breve texto contando para os empreendedores que lhe contrataram o que você tem em mente para tal projeto. Como será esse celular, como ele funcionaria, quais seriam as suas características, etc. 

## Problema 4

Como você explicaria o que é um Banco de dados para seu sobrinho de 8 anos?


## Problema 5

A Everest Companhias aéreas resolveu contratar um analista de sistemas para desenvolver um sistema de reserva de passagens para os seus clientes. A companhia dispõe de 3 tipos de aviões, 1 Boeing 737-600, 1 Boeing 787-10 e 1 Boeing 777-200 onde cada um possui uma capacidade máxima de 242, 250 e 440 passageiros, respectivamente, e uma capacidade máxima de combustível de 26.000, 138.000 e 117.000 litros, respectivamente. Um voo pode fazer várias escalas em diversas cidades desde a origem até o destino para embarque/desembarque e abastecimento da aeronave, e cada percurso entre as cidades é chamado de trecho. Em reunião com a diretoria o analista de sistemas responsável pelo levantamento dos requisitos ficou decidido que para realização da reserva o cliente precisa informar seus dados pessoais (nome, e-mail, CPF, data de nascimento), a quantidade de passagens que deseja, a cidade de origem, cidade de destino, a data de ida e retorno (se houver). O voo possui uma numeração de 4 dígitos que o identifica em um único dia (ex.: o voo 3494 vai de Salvador a Porto Alegre com escala em São Paulo todos os dias). O processo funciona da seguinte forma: O cliente pesquisa por uma cidade de origem e destino, informa a data e o sistema retorna uma listagem dos voos disponíveis para escolha do cliente. Para que a reserva seja efetuada é necessário que haja lugares disponíveis em todos os trechos do voo escolhido. Pela legislação da ANAC é permitida a venda de 10% de passagens além da capacidade do avião por overbooking¹.

¹. É um termo usado pelas companhias para referir-se a prática de vender um serviço em quantidade maior do que a capacidade que a empresa pode fornecer.

__Exercício__: você é um desenvolvedor da equipe do analista citado no problema acima e o responsável por fazer a modelagem das classes e desenvolver o serviço que irá realizar a reserva do cliente no voo desejado.

Observações:

1. Não se preocupe com o banco de dados. Faça apenas a modelagem das classes e as popule com dados em memória (hardcode);
3. Implemente uma única tela de entrada de dados para alimentar o serviço de reserva.


Sucesso!